---
title : "KNOW MORE <br> ABOUT ME"
image : "images/backgrounds/portrait.jpg"
# button
button:
  enable : true
  label : "Connect"
  link : "https://www.linkedin.com/in/prashanga-dhakal-0a4677a6/"

########################### Experience ##############################
experience:
  enable : true
  title : "EXPERIENCE"
  experience_list:   
    # experience item loop
    - name : "Web Developer Intern"
      company : "AusNep IT Solutions"
      duration : "Feb - April 2021"
      content : "<ul>
  <li>Participate in code reviews </li>
  <li>Fix bugs in legacy applications </li>
  <li>Test and review use cases </li>
  <li>Assist in creating marketing materials for social networks </li>
  <li>Participated in UI design meetings and reviews </li>
</ul>
"

    # experience item loop
    - name : "Volunteer Web Developer"
      company : "Code.Sydney"
      duration : "2019-2021"
      content : "<ul>
  <li>Build small projects in a team led by professional mentors  </li>
  <li>Participate in regular online meetings and in the slack community channel </li>
  <li>Allocate few hours each week to learn and practice coding </li>
  <li>Perform code reviews and create pull requests to be reviewed </li>
  <li>Participate in UX / UI design meetings and reviews </li>
</ul>"
      
  
    # experience item loop
    - name : "Masters in Information Techonology"
      company : "Victoria University Sydney"
      duration : "2018-2019"
      content : ""

############################### Skill #################################
skill:
  enable : true
  title : "SKILL"
  skill_list:
    # skill item loop
    - name : "Web Development"
      percentage : "100%"
      
    # skill item loop
    - name : "Graphic Design"
      percentage : "70%"
      
    # skill item loop
    - name : "Responsive Web Design"
      percentage : "80%"


# custom style
custom_class: "" 
custom_attributes: "" 
custom_css: ""
---

Hi I'm Prashanga Dhakal, a Web Developer based in Sydney, Australia. <br>
I like to build web applications and work on interesting projects. <br>
If you'd like to receive my service, collaborate or just network, feel free to contact me via the contact link, or connect with me on LinkedIn
