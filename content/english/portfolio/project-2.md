---
title: "Pokedex Lite"
date: 2020-10-18T12:14:34+06:00
image: "images/portfolio/item2.png"
description: "This application is a simple pokedex"
categories: ["Vue/Quasar"]
draft: false
project_info:
- name: "Project Link"
  icon: "fas fa-link"
  content: "https://pokedexlite.netlify.app/"
---
The project was required as the final showcase project of the Quasar Learning Program organised by Code.Sydney. I choose to build the Pokémon project to learn how to use large scale API from a third party and also implement the newly acquired skills and knowledge of the Quasar framework. The application was built using the Quasar framework and pokeapi (https://pokeapi.co/) as the Pokémon API for this project. 