---
title: "JavaScript algorithms and Data Structure"
date: 2019-12-27T12:14:34+06:00
image: "images/portfolio/item9.png"
description: "This is meta description."
categories: ["Certifications"]
draft: false
project_info:
- name: "Project Link"
  icon: "fas fa-link"
  content: "https://www.freecodecamp.org/certification/prashanga/responsive-web-design"
---
This certification is the proof of completion of the Responsive Web Design course offered by freecCodeCamp.