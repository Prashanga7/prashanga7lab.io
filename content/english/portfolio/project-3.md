---
title: "TDD Battleship"
date: 2021-01-15T12:14:34+06:00
image: "images/portfolio/item3.jpg"
description: "Application Built to study test driven development - From Odin Project"
categories: ["HTML/CSS/JS"]
draft: false
project_info:
- name: "Project Link"
  icon: "fas fa-link"
  content: "https://jovial-wing-a00ef8.netlify.app/"
---
The project is an assignment from the Full Stack Javascript course from Odin Project. The project is built using the Test Driven Development approach.