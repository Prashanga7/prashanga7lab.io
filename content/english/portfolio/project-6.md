---
title: "Tic-tac-toe"
date: 2020-11-16T12:14:34+06:00
image: "images/portfolio/item6.png"
description: "This is meta description."
categories: ["HTML/CSS/JS"]
draft: false
project_info:
- name: "Project Link"
  icon: "fas fa-link"
  content: "https://optimistic-wright-f9ac31.netlify.app/"
---
This Tic-tac-toe game was built as an assignment of the Odin Project Full Stack Javascript course. It was built using vanilla JS,HTML and CSS.