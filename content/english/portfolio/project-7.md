---
title: "Fullstack Web Development Certification"
date: 2020-03-01T12:14:34+06:00
image: "images/portfolio/item7.png"
description: "This is meta description."
categories: ["Certifications"]
draft: false
project_info:
- name: "Project Link"
  icon: "fas fa-link"
  content: "https://github.com/Prashanga/FullStackOpen-Part-3-4-5-6-7/tree/master"
---
This certification is a proof of completion of the Fullstack Javascript coursed offered by the University of Helsinki, which is available at https://fullstackopen.com/en/ <br>
The course was completed on March 2020 and I have put most of the assignments in different branches of the Github repo available at https://github.com/Prashanga/FullStackOpen-Part-3-4-5-6-7/tree/master