---
title: "JavaScript lgorithms and Data Structure"
date: 2019-12-20T12:14:34+06:00
image: "images/portfolio/item8.png"
description: "This is meta description."
categories: ["Certifications"]
draft: false
project_info:
- name: "Project Link"
  icon: "fas fa-link"
  content: "https://www.freecodecamp.org/certification/prashanga/javascript-algorithms-and-data-structures"
---
This certification is the proof of completion of the JavaScript Algorithms and Data Structures course offered by freecCodeCamp.