---
title: "Simple Library App"
date: 2020-11-03T12:14:34+06:00
image: "images/portfolio/item4.png"
description: "This is meta description."
categories: ["HTML/CSS/JS"]
draft: false
project_info:
- name: "Project Link"
  icon: "fas fa-link"
  content: "https://cranky-wiles-fcd6e8.netlify.app"
---
This is a simple page built using vanilla JS, HTML and CSS