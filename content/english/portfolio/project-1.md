---
title: "The Breather App"
date: 2020-08-02T12:14:34+06:00
image: "images/portfolio/item1.png"
description: "This is meta description."
categories: ["Vue/Quasar"]
draft: false
project_info:
- name: "Project Link"
  icon: "fas fa-link"
  content: "https://thebreather.netlify.app/"
---
The objective of this project was to implement the skills learned from the VueJs Learning Program from Code.Sydney. I choose to build this project to create something which is functional and relevant to real-world use. The tools used in this application were Vue and Vuetify